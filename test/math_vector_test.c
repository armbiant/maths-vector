/**
 * @file
 * @author Martin Stejskal
 * @brief Tests for "math vector" module
 */
// ===============================| Includes |================================
#include "../math_vector.h"

#include <assert.h>
#include <math.h>
#include <stdio.h>
// ================================| Defines |================================
#define PRINT_ERR(expected_value, current_value)                         \
  printf("%s : %d : Expected value: %d | current value: %d\n", __func__, \
         __LINE__, (int)expected_value, (int)current_value)
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================
int32_t mvt_scale_vect_short_test(void);
int32_t mvt_check_component_range(void);
// ===============| Internal function prototypes: high level |================
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================
static void _report_if_not_match(int32_t *pi32_num_of_errors,
                                 int32_t i32_expected, int32_t i32_current);

// =========================| High level functions |==========================
int main(void) {
  int32_t i32_num_of_errors = 0;

  i32_num_of_errors += mvt_scale_vect_short_test();
  i32_num_of_errors += mvt_check_component_range();

  ///@todo More tests

  if (i32_num_of_errors) {
    printf("Something went wrong. Total number of errors: %d\n",
           i32_num_of_errors);
  } else {
    printf("No error reported (^_^)\n");
  }

  return i32_num_of_errors;
}
// ========================| Middle level functions |=========================
int32_t mvt_scale_vect_short_test(void) {
  int32_t i32_num_of_errors = 0;

  ts_mv_vector_32 s_result;
  ts_mv_vector_32 s_source = {.i32_x = 100, .i32_y = 300, .i32_z = 500};

  // Scale ratio should be 1:2 (50%)
  int32_t i32_scale_source = 1000;
  int32_t i32_scale_target = 500;

  te_math_vect_err e_err_code = mv_vect_32_scale_symetric(
      &s_result, s_source, i32_scale_source, i32_scale_target);

  // Error code should be 0
  if (e_err_code) {
    i32_num_of_errors += 1;
    PRINT_ERR(0, e_err_code);
  }

  // Compare expected value with result
  _report_if_not_match(&i32_num_of_errors, s_source.i32_x / 2, s_result.i32_x);
  _report_if_not_match(&i32_num_of_errors, s_source.i32_y / 2, s_result.i32_y);
  _report_if_not_match(&i32_num_of_errors, s_source.i32_z / 2, s_result.i32_z);

  return i32_num_of_errors;
}

int32_t mvt_check_component_range(void) {
  int32_t i32_num_of_errors = 0;

  ts_mv_vector_16 s_vect = {
      .i16_x = 90,
      .i16_y = 150,
      .i16_z = -63,
  };

  bool b_res = mv_vect_16_are_components_in_range(&s_vect, -63, 150);

  // Should be in range
  _report_if_not_match(&i32_num_of_errors, 1, b_res);

  b_res = mv_vect_16_are_components_in_range(&s_vect, -62, 200);
  // Should not be in range
  _report_if_not_match(&i32_num_of_errors, 0, b_res);

  return i32_num_of_errors;
}
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
static void _report_if_not_match(int32_t *pi32_num_of_errors,
                                 int32_t i32_expected, int32_t i32_current) {
  if (i32_current != i32_expected) {
    *pi32_num_of_errors += 1;
    PRINT_ERR(i32_expected, i32_current);
  }
}
